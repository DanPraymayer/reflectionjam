﻿Shader "Sprites/Night" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}
		SubShader{
		Pass{
		CGPROGRAM

#pragma glsl
#pragma vertex vert_img
#pragma fragment frag

#include "UnityCG.cginc"

		uniform sampler2D _MainTex;

		uniform int _LightsLength = 0;
		uniform float4 _Lights [50];
		uniform float _LightSizes [50];

		uniform float _AspectRatio;
		
	float4 frag(v2f_img img) : COLOR{
		float4 c = tex2D(_MainTex, img.uv);
		float2 ratio = float2(1, 1 / _AspectRatio);
		float delta = 0;


		for (int i = 0; i < _LightsLength; i++)
		{
			float ray = length((_Lights[i].xy - img.uv.xy) * ratio / _LightSizes[i]);
			delta += smoothstep(_Lights[i].z, 0.1, ray) * _Lights[i].w;
		}


		c.rgb *= delta;
		return c;
	}
		ENDCG
	}
	}
}