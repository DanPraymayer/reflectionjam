﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float moveSpeed;
    bool shieldSwing;
    bool canSwing =true;
    public float swingThreshold;
    public float swingCooldown;
    public float swingDuration;
    float timer;

    Animator m_animator;
    SpriteRenderer m_spriteRenderer;

    Rigidbody2D m_rigidbody;
    BoxCollider2D m_collider;
    Vector2 m_velocity;
    Vector3 m_shieldVector;

	// Use this for initialization
	void Start () {
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_collider = GetComponent<BoxCollider2D>();
        m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_animator = GetComponentInChildren<Animator>();
    }

	// Update is called once per frame
	void Update () {
        m_velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        m_shieldVector = new Vector2(Input.GetAxis("HorizontalFire"), -Input.GetAxis("VerticalFire"));

        if (m_shieldVector.magnitude > swingThreshold && canSwing)
        {
            shieldSwing = true;
            m_collider.size = new Vector3(2,1.5f);
            canSwing = false;
            StartCoroutine(Timer(swingDuration));
        }
        else
        {
            m_collider.size = new Vector3(1.12f, .75f);
        }

        m_spriteRenderer.flipX = m_velocity.x < 0.1;

        m_animator.SetFloat("Speed", Mathf.Abs(m_velocity.magnitude));
        m_animator.SetBool("Spin", shieldSwing);
    }

    private void FixedUpdate()
    {
        
        m_rigidbody.velocity = !shieldSwing ? m_velocity * moveSpeed: Vector2.zero;
    }

    public void OrbHit(Bullet bullet)
    {
        if (shieldSwing) {
            RedirectOrb(bullet);
        } else {
            m_rigidbody.velocity = Vector3.zero;
            transform.position = new Vector3(CheckPointManager.currentCheckPoint.transform.position.x, CheckPointManager.currentCheckPoint.transform.position.y, transform.position.z);
        }
    }

    void RedirectOrb(Bullet bullet)
    {
        bullet.transform.position = transform.position + m_shieldVector;
        bullet.direction = m_shieldVector;
        bullet.speed *= 1.25f;
    }

    IEnumerator Timer(float time)
    {
        timer = time;

        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }

        if (shieldSwing)
        {
            shieldSwing = false;
            StartCoroutine(Timer(swingCooldown));
        }
        else
        {
            canSwing = true;
        }

        yield return null;
    }
}
