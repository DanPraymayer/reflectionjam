﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour {

    public static CheckPointScript currentCheckPoint;

    public static void UpdateCheckPoint(CheckPointScript cps, int id)
    {
        if (id < 0) {
            Debug.LogWarning("CheckPoint number is invalid. Check Inspector value.", cps.gameObject);
            return;
        }

        if (id == 0 || id > currentCheckPoint.checkPointNumber) {
            currentCheckPoint = cps;
        }
    }
}
