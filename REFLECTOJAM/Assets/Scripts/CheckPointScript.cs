﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointScript : MonoBehaviour
{
    public bool finalCheckPoint = false;
    public int checkPointNumber = -1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (finalCheckPoint)
            LevelManHandler.DemandNextLevel();

        if (CheckPointManager.currentCheckPoint != this)
            if (other.transform.CompareTag("Player"))
                CheckPointManager.UpdateCheckPoint(this, checkPointNumber);
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (CheckPointManager.currentCheckPoint != this)
            if (other.transform.CompareTag("Player"))
                CheckPointManager.UpdateCheckPoint(this, checkPointNumber);
    }
}
