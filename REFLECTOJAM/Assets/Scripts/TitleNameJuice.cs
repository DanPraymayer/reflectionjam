﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//AKA Juice for the juice god

public class TitleNameJuice : MonoBehaviour
{

    Vector3 startPos, endPos;
    public Vector2 bounceDist = new Vector2(2, 2);
    Text myText;
    Color targetCol;
    bool increasing;
    public float colorChangeMultiplier = 10, jiggleChangeTimer = 0.25f;

    // Use this for initialization
    void Start()
    {
        myText = GetComponent<Text>();
        startPos = transform.position;
        targetCol = myText.color;
        StartCoroutine(FakeJiggle());
    }

    // Update is called once per frame
    void Update()
    {
        if (myText.color == targetCol)
        {
            if (increasing)
            {
                targetCol = new Color(Mathf.Clamp(myText.color.r + Random.Range(-0.25f,0), 0.4f, 1), myText.color.g, myText.color.b, Random.Range(0.75f, 1));
                increasing = false;
            }
            else
            {
                targetCol = new Color(Mathf.Clamp(myText.color.r + Random.Range(0, 0.25f), 0.4f, 1), myText.color.g, myText.color.b, Random.Range(0.75f, 1));
                increasing = true;
            }
        }
        else
        {
            myText.color = Color.Lerp(myText.color, targetCol, Time.deltaTime * colorChangeMultiplier);
        }
    }

    IEnumerator FakeJiggle()
    {
        while (gameObject.activeInHierarchy)
        {
            transform.position = startPos + new Vector3(Random.Range(-bounceDist.x, bounceDist.x), Random.Range(-bounceDist.y, bounceDist.y));
            yield return new WaitForSeconds(jiggleChangeTimer);
        }
    }
}
