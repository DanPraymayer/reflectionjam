﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour {

    public List<Bullet> pooledObjects;
    public GameObject objectToPool;
    public int amountToPool;

    public static BulletPool SharedInstance;

    void Awake()
    {
        SharedInstance = this;
        CreateBullets();
    }

    void CreateBullets()
    {
        pooledObjects = new List<Bullet>();
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = (GameObject)Instantiate(objectToPool,this.transform);
            pooledObjects.Add(obj.GetComponent<Bullet>());
            obj.SetActive(false);
           
        }
    }

    public Bullet GetPooledObject()
    {
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if (!pooledObjects[i].gameObject.activeInHierarchy)
            {
                return pooledObjects[i];
            }
        } 
        return null;
    }

}
