﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManHandler : MonoBehaviour
{
    public static LevelManHandler LMH;
    public int goToLevel = -1;

    void Awake() {
        if (LMH == null)
            LMH = this;
    }

    public void LoadLevel()
    {
        if (goToLevel == -1)
            LoadLevel(0);
        else
            LoadLevel(goToLevel);
    }

    public void LoadLevel(int level)
    {
        SceneManager.LoadScene(level);
    }

    public void QuitGame()
    {
        Debug.Break();
        Application.Quit();
    }

    public static void DemandNextLevel() {
        LMH.LoadLevel();
    }
}
