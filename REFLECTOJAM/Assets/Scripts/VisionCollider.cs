﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisionCollider : MonoBehaviour {

    public ShooterOfBullets root;

	void OnTriggerEnter2D (Collider2D other) {
        if (other.CompareTag("Player"))
        {
            root.Target = other.transform;
            root.shouldShoot = true;
        }
	}

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            root.Target = null;
            root.shouldShoot = false;
        }
    }
}
