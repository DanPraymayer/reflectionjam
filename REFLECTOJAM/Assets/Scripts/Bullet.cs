﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public Rigidbody2D m_rigidbody;

    public Vector2 direction;

    public float speed, damper, size;

    void Awake()
    {
        m_rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Prep(Transform startSetting, Vector2 dir, float speed, float damper, float size)
    {
        transform.position = startSetting.position;
        transform.rotation = startSetting.rotation;

        this.speed = speed;
        this.damper = damper;
        this.size = size;
        direction = dir;

    }

    private void FixedUpdate()
    {
        m_rigidbody.velocity = direction * speed;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.transform.CompareTag("KillsBullets"))
        {
            gameObject.SetActive(false);
        }
        if (coll.transform.CompareTag("Player"))
        {
            //gameObject.SetActive(false);
            coll.gameObject.GetComponent<Player>().OrbHit(this);
        }
    }
}
