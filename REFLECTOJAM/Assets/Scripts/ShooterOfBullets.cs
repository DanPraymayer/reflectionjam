﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterOfBullets : MonoBehaviour
{

    public float bulletSpeed;
    public float bulletDamper;
    public float lightSize;

    public int simultaneousEmitAmt = 1;
    public float aimDeviation = 0, rotateBy = 3;
    public bool aimedAtPlayer, useDeviationAsRandom, canShoot, shouldShoot;


    public GameObject bulletPrefab;
    public Transform Target;

    bool shooting;
    public float FireRateCooldown = 0.5f;

    private void Start()
    {
        StartCoroutine(ShotCooldown());

    }

    void Update()
    {
        if (!shooting && shouldShoot)
        {
            if (aimedAtPlayer)
            {
                if (Target == null)
                {
                    shouldShoot = false;
                    return;
                }
                else
                {
                    transform.right = (Vector2)(Target.position - transform.position);
                    ShootBulls();
                }
            }
            else
            {
                transform.Rotate(new Vector3(0, 0, rotateBy));
                ShootBulls();
            }

            StartCoroutine(ShotCooldown());
        }
    }

    void ShootBulls()
    {
        float f = 0;

        for (int i = 0; i < simultaneousEmitAmt; i++)
        {
            if (useDeviationAsRandom)
            {
                f = Random.Range(-aimDeviation, aimDeviation);
            }
            else
            {
                if (simultaneousEmitAmt % 2 == 0) //even
                {
                    f = aimDeviation - ((i) * ((aimDeviation * 2) / (simultaneousEmitAmt - 1)));
                }
                else //odd
                {
                    if (i == 0)
                    {
                        f = 0;
                    }
                    else
                    {
                        int lowCap = Mathf.FloorToInt(simultaneousEmitAmt / 2);
                        if (i <= lowCap)//left half
                            f = -aimDeviation + ((i - 1) * (aimDeviation / Mathf.FloorToInt(simultaneousEmitAmt / 2)));
                        else
                            f = aimDeviation - ((i - lowCap - 1) * (aimDeviation / Mathf.FloorToInt(simultaneousEmitAmt / 2)));
                    }
                }
            }

            ShootBulls2(f);
        }
    }

    void ShootBulls2(float angle)
    {
        Vector2 dir = new Vector2(Mathf.Cos(Mathf.Deg2Rad * (angle + transform.rotation.eulerAngles.z)), Mathf.Sin(Mathf.Deg2Rad * (angle + transform.rotation.eulerAngles.z)));
        Bullet newBullet = NextAvailableBullet();
        newBullet.Prep(transform, dir, bulletSpeed, bulletDamper,lightSize);
        newBullet.gameObject.SetActive(true);
    }

    Bullet NextAvailableBullet()
    {
        return BulletPool.SharedInstance.GetPooledObject();
    }

    IEnumerator ShotCooldown()
    {
        shooting = true;
        yield return new WaitForSeconds(FireRateCooldown);
        shooting = false;
    }
}
