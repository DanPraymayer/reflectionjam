﻿using UnityEngine;
using System.Collections;

public class Darkness : MonoBehaviour
{

    public Material material;

    public Camera m_Camera;

    Vector4[] lights;
    float[] lightSize;

    public BulletPool bullets;

    // global reference, this is where all game data is stored


    private void Start()
    {
        lights = new Vector4[bullets.pooledObjects.Count];
        lightSize = new float[bullets.pooledObjects.Count];

        material.SetVectorArray("_Lights", lights);
        material.SetFloatArray("_LightSizes", lightSize);

        material.SetFloat("_AspectRatio", m_Camera.aspect);
        material.SetInt("_LightsLength", bullets.pooledObjects.Count);
        
    }

    void CreateLightVectorArray()
    {
        int counter = 0;
        foreach (Bullet b in bullets.pooledObjects)
        {
            if (b == null)
            {
                ++counter;
                continue;
            }
            if (!b.gameObject.activeInHierarchy)
            {
                // ensure light is turned off for dead players
                lights[counter] = Vector4.zero;
                lightSize[counter] = 0;
                ++counter;
                continue;
            }
            // get the position of the mech
            Vector2 pos = b.transform.position;

            // transform object world position to screen position
            Vector4 v1 = Camera.main.WorldToViewportPoint(pos);
            // set light distance (the radius of the virtual circle, hardcoded)
            v1.z = 0.15f;
            // enable the light
            v1.w = 1;
            lights[counter] = v1;
            lightSize[counter] = b.size;
            ++counter;
        }
    }

    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        CreateLightVectorArray();
        material.SetVectorArray("_Lights", lights);
        material.SetFloatArray("_LightSizes", lightSize);

        Graphics.Blit(source, destination, material);
    }
}
